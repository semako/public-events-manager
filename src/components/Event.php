<?php

namespace semako\eventsManager\components;

use semako\eventsManager\interfaces\IEvent;
use yii\base\Event as BaseEvent;

/**
 * Class Event
 * @package semako\eventsManager\components
 */
abstract class Event extends BaseEvent implements IEvent
{
    /**
     * @var mixed
     */
    public $message;

    /**
     * Event constructor.
     * @param mixed $message
     */
    public function __construct($message)
    {
        $this->message = $message;
        parent::__construct();
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return object
     */
    public function getSender()
    {
        return $this->sender;
    }

    /**
     * @return bool
     */
    public function getHandled()
    {
        return $this->handled;
    }

    /**
     * @return mixed
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * @return mixed
     */
    public function getMessage()
    {
        return $this->message;
    }
}
