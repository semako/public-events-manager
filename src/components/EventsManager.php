<?php

namespace semako\eventsManager\components;

use semako\eventsManager\interfaces\IEvent;
use semako\eventsManager\interfaces\IEventsManager;
use yii;
use yii\base\Component;

/**
 * Class EventsManager
 *
 * For using of this module, add to config, components section:
 * 'eventsManager' => [
 *     'class' => 'semako\eventsManager\components\EventsManager',
 * ]
 *
 * Than, use trait 'semako\eventsManager\traits\EventsManager' in Application class
 *
 * To trigger an event:
 * Yii::$app->getEventsManager()->raise(<EventNameConstant>, <ObjectWhichImplementsIEvent>);
 *
 * Example event constant
 * ====================================
 * const ON_TASKER_GET_GROUP_STATS_SUCCESS = 'onTaskerGetGroupStatsSuccess';
 *
 * Example implementation of register()
 * ====================================
 * private function register()
 * {
 *     Yii::$app->on(self::ON_TASKER_GET_GROUP_STATS_SUCCESS, function (OnGetGroupStatsSuccess $event) {
 *         Yii::getLogger()->log('Success: ' . Json::encode($event->getMessage()->getAttributes()), Logger::LEVEL_ERROR);
 *     });
 * }
 *
 * @package app\components
 */
abstract class EventsManager extends Component implements IEventsManager
{
    /**
     * @var IEventsManager[]
     */
    private $modules;

    /**
     *
     */
    public function init()
    {
        $this->register();
    }

    /**
     * @param $name
     * @param IEvent|null $event
     */
    protected function raise($name, IEvent $event = null)
    {
        Yii::$app->trigger($name, $event);
    }

    /**
     * @param string $name
     * @param IEventsManager $manager
     */
    protected function addModule($name, IEventsManager $manager)
    {
        $this->modules[$name] = $manager;
    }

    /**
     * @param string $name
     * @return bool
     */
    public function moduleExists($name)
    {
        return isset($this->modules[$name]);
    }

    /**
     * @param string $name
     * @param array $params
     * @return mixed|IEventsManager
     */
    public function __call($name, $params)
    {
        if (isset($this->modules[$name])) {
            return $this->modules[$name];
        } else {
            return parent::__call($name, $params);
        }
    }

    /**
     * @return void
     */
    abstract protected function register();
}
