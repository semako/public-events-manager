<?php

namespace semako\eventsManager\interfaces;

/**
 * Interface IEvent
 * @package semako\eventsManager\interfaces
 */
interface IEvent
{
    /**
     * @return string
     */
    public function getName();

    /**
     * @return object
     */
    public function getSender();

    /**
     * @return bool
     */
    public function getHandled();

    /**
     * @return mixed
     */
    public function getData();

    /**
     * @return mixed
     */
    public function getMessage();
}
