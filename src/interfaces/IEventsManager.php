<?php

namespace semako\eventsManager\interfaces;

/**
 * Interface IEventsManager
 * @package semako\eventsManager\interfaces
 */
interface IEventsManager
{
    /**
     * @return string
     */
    public static function getModuleName();

    /**
     * @param string $name
     * @return bool
     */
    public function moduleExists($name);
}
