<?php

namespace semako\eventsManager\traits;

use semako\eventsManager\interfaces\IEventsManager;
use yii;

/**
 * Class EventsManager
 * @package semako\eventsManager\traits
 * @property IEventsManager $eventsManager
 */
trait EventsManager
{
    /**
     * @return IEventsManager
     */
    public function getEventsManager()
    {
        return Yii::$app->eventsManager;
    }
}
